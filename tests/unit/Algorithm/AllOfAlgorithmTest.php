<?php


namespace Hammie\Algorithms\Tests\Unit\Algorithm;


use ArrayIterator;
use Hammie\Algorithms\Algorithm\AllOfAlgorithm;
use Hammie\Algorithms\Predicate\ValuePredicate;
use PHPUnit\Framework\TestCase;

class AllOfAlgorithmTest extends TestCase
{
    function test_it_returns_true_if_all_values_match(): void
    {
        $input = new ArrayIterator([true, true, true]);

        $result = (new AllOfAlgorithm())($input, new ValuePredicate(true));

        $this->assertTrue($result);
    }

    function test_it_returns_false_if_any_values_differ(): void
    {
        $input = new ArrayIterator([1, 1, 2]);

        $result = (new AllOfAlgorithm())($input, new ValuePredicate(1));

        $this->assertFalse($result);
    }
}
