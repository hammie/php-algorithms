<?php


namespace Hammie\Algorithms\Tests\Unit\Algorithm;


use ArrayIterator;
use Closure;
use Hammie\Algorithms\Algorithm\ReduceAlgorithm;
use PHPUnit\Framework\TestCase;

class ReduceAlgorithmTest extends TestCase
{
    protected static function accumulateReduction(): Closure
    {
        return function (int $carry, int $value): int {
            return $carry + $value;
        };
    }

    function test_it_accumulates_values(): void
    {
        $input = new ArrayIterator([0, 1, 2, 3, 4, 5]);

        $result = (new ReduceAlgorithm())($input, 0, static::accumulateReduction());

        $this->assertSame(15, $result);
    }
}
