<?php


namespace Hammie\Algorithms\Tests\Unit\Predicate;


use Hammie\Algorithms\Predicate\ValuePredicate;
use PHPUnit\Framework\TestCase;

class ValuePredicateTest extends TestCase
{
    function test_it_returns_true_for_matching_value(): void
    {
        $input = 5;
        $predicate = new ValuePredicate(5);

        $result = $predicate($input);

        $this->assertTrue($result);
    }

    function test_it_returns_false_for_different_value(): void
    {
        $input = 6;
        $predicate = new ValuePredicate(5);

        $result = $predicate($input);

        $this->assertFalse($result);
    }
}
