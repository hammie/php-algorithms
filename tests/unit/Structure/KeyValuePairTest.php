<?php


namespace Hammie\Algorithms\Tests\Unit\Structure;


use Hammie\Algorithms\Structure\KeyValuePair;
use PHPUnit\Framework\TestCase;

class KeyValuePairTest extends TestCase
{
    function test_it_takes_key_and_value_in_constructor(): void
    {
        $result = new KeyValuePair('key', 'value');

        $this->assertSame('key', $result->key);
        $this->assertSame('value', $result->value);
    }
}