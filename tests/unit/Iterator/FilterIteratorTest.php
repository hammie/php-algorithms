<?php

namespace Hammie\Algorithms\Tests\Unit\Iterator;

use ArrayIterator;
use Hammie\Algorithms\Iterator\FilterIterator;
use PHPUnit\Framework\TestCase;

class FilterIteratorTest extends TestCase
{
    /**
     * @return callable(int):bool
     */
    protected static function isEvenPredicate(): callable {
        return function (int $number) {
            return $number % 2 === 0;
        };
    }

    function test_it_filters_data_in_array(): void
    {
        $input = new ArrayIterator([1, 2, 3, 4, 5]);

        $iterator = new FilterIterator($input, static::isEvenPredicate());
        $result = iterator_to_array($iterator);

        $this->assertEquals([1 => 2, 3 => 4], $result);
    }

    function test_it_does_not_modify_input_iterator(): void
    {
        $input = new ArrayIterator([0, 1, 2, 3, 4, 5]);

        $iterator = new FilterIterator($input, static::isEvenPredicate());
        iterator_to_array($iterator);

        $this->assertFalse($iterator->valid());
        $this->assertSame(0, $input->current());
    }

    function test_it_does_not_share_iterator_with_clones(): void
    {
        $input = new ArrayIterator([0, 1, 2, 3, 4, 5]);

        $iterator = new FilterIterator($input, static::isEvenPredicate());
        $clone = clone $iterator;
        iterator_to_array($iterator);

        $this->assertFalse($iterator->valid());
        $this->assertSame(0, $clone->current());
    }
}
