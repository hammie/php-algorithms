<?php


namespace Hammie\Algorithms\Tests\Unit\Iterator;


use ArrayIterator;
use Hammie\Algorithms\Iterator\ZipIterator;
use Hammie\Algorithms\Structure\KeyValuePair;
use PHPUnit\Framework\TestCase;

class ZipIteratorTest extends TestCase
{
    function test_it_converts_list_pairs_to_map(): void
    {
        $input = new ArrayIterator([new KeyValuePair('first', 1), new KeyValuePair('second', 2), new KeyValuePair('third', 3)]);
        assert($input instanceof \Iterator);

        $iterator = new ZipIterator($input);
        $result = iterator_to_array($iterator);

        $this->assertSame(['first' => 1, 'second' => 2, 'third' => 3], $result);
    }
}
