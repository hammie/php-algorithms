<?php


namespace Hammie\Algorithms\Tests\Unit\Iterator;


use ArrayIterator;
use Hammie\Algorithms\Iterator\TransformIterator;
use PHPUnit\Framework\TestCase;

class TransformIteratorTest extends TestCase
{
    /**
     * @return callable(mixed):string
     */
    protected static function toStringTransform(): callable
    {
        return function ($value) {
            return (string) $value;
        };
    }

    function test_it_maps_numbers_to_strings(): void
    {
        $input = new ArrayIterator([0, 1, 2]);

        $iterator = new TransformIterator($input, static::toStringTransform());
        $result = iterator_to_array($iterator);

        $this->assertSame(['0', '1', '2'], $result);
    }

    function test_it_does_not_modify_input_iterator(): void
    {
        $input = new ArrayIterator([0, 1, 2]);

        $iterator = new TransformIterator($input, static::toStringTransform());
        iterator_to_array($iterator);

        $this->assertFalse($iterator->valid());
        $this->assertSame(0, $input->current());
    }

    function test_it_does_not_share_iterator_with_clones(): void
    {
        $input = new ArrayIterator([0, 1, 2]);

        $iterator = new TransformIterator($input, static::toStringTransform());
        $clone = clone $iterator;
        iterator_to_array($iterator);

        $this->assertFalse($iterator->valid());
        $this->assertSame('0', $clone->current());
    }
}
