<?php


namespace Hammie\Algorithms\Tests\Unit\Iterator;


use ArrayIterator;
use Hammie\Algorithms\Iterator\UnzipIterator;
use Hammie\Algorithms\Structure\KeyValuePair;
use PHPUnit\Framework\TestCase;

class UnzipIteratorTest extends TestCase
{
    function test_it_converts_map_to_list_of_key_value_pairs(): void
    {
        $input = new ArrayIterator(['first' => 1, 'second' => 2, 'third' => 3]);

        $iterator = new UnzipIterator($input);
        $result = iterator_to_array($iterator);

        $this->assertEquals(
            [
                'first' => new KeyValuePair('first', 1),
                'second' => new KeyValuePair('second', 2),
                'third' => new KeyValuePair('third', 3),
            ],
            $result
        );
    }
}
