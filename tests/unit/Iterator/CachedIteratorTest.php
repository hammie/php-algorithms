<?php


namespace Hammie\Algorithms\Tests\Unit\Iterator;


use Generator;
use Hammie\Algorithms\Iterator\CachedIterator;
use PHPStan\Testing\TestCase;

class CachedIteratorTest extends TestCase
{
    /**
     * @param int $limit
     * @return Generator<int>
     */
    protected static function countTo(int $limit)
    {
        for($i = 0; $i < $limit; ++$i) {
            yield $i + 1;
        }
    }

    function test_it_caches_generator(): void
    {
        $input = new CachedIterator(static::countTo(5));

        $this->assertSame(0, $input->key());
        $this->assertSame(1, $input->current());
        $input->next();
        $this->assertSame(1, $input->key());
        $this->assertSame(2, $input->current());
        $input->next();
        $this->assertSame(2, $input->key());
        $this->assertSame(3, $input->current());

        $input->rewind();

        $this->assertSame(0, $input->key());
        $this->assertSame(1, $input->current());
        $input->next();
        $this->assertSame(1, $input->key());
        $this->assertSame(2, $input->current());
    }
}
