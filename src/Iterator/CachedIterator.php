<?php


namespace Hammie\Algorithms\Iterator;


use Hammie\Algorithms\Structure\KeyValuePair;
use Iterator;

/**
 * @template TKey
 * @template TValue
 * @implements Iterator<TKey, TValue>
 */
class CachedIterator implements Iterator
{
    /**
     * @var Iterator<TKey, TValue>
     */
    protected $iterator;

    /**
     * @var int
     */
    protected $index = 0;

    /**
     * @var array<int, KeyValuePair<TKey, TValue>>
     */
    protected $cache;

    /**
     * @var string
     */
    protected $state = self::STATE_LIVE;

    private const STATE_LIVE = 'live';
    private const STATE_CACHE = 'cache';

    /**
     * CacheIterator constructor.
     * @param Iterator<TKey, TValue> $iterator
     */
    public function __construct(Iterator $iterator)
    {
        $this->iterator = $iterator;
    }

    public function __clone()
    {
        $this->iterator = clone $this->iterator;
    }

    public function next(): void
    {
        $this->index++;
        if ($this->index >= count($this->cache)) {
            $this->iterator->next();
            $this->cache[$this->index] = new KeyValuePair($this->iterator->key(), $this->iterator->current());
        }
    }

    public function valid(): bool
    {
        if ($this->state === self::STATE_LIVE) {
            return $this->iterator->valid();
        }
        else {
            return true;
        }
    }

    public function rewind(): void
    {
        $this->index = 0;
    }

    /**
     * @return TValue
     */
    public function current()
    {
        $this->updateCache();
        return $this->cache[$this->index]->value;
    }

    /**
     * @return TKey
     */
    public function key()
    {
        $this->updateCache();
        return $this->cache[$this->index]->key;
    }

    protected function updateCache(): void
    {
        if ($this->state == self::STATE_LIVE) {
            $this->cache[$this->index] = new KeyValuePair($this->iterator->key(), $this->iterator->current());
            $this->state = self::STATE_CACHE;
        }
    }
}