<?php
/**
 * Copyright (c) 2019 Sam Hambrouck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace Hammie\Algorithms\Iterator;


use Iterator;

/**
 * @template TKey
 * @template TValue
 * @template TResult
 * @implements Iterator<TKey, TResult>
 */
class TransformIterator implements Iterator
{
    /**
     * @var Iterator<TKey, TValue>
     */
    protected $iterator;

    /**
     * @var callable(TValue):TResult
     */
    protected $transform;

    /**
     * @param Iterator<TKey, TValue> $iterable
     * @param callable(TValue):TResult $transform
     */
    public function __construct(Iterator $iterable, callable $transform)
    {
        $this->iterator = clone $iterable;
        $this->transform = $transform;
    }

    public function __clone()
    {
        $this->iterator = clone $this->iterator;
    }

    /**
     * @return TResult
     */
    public function current()
    {
        return ($this->transform)($this->iterator->current());
    }

    public function next(): void
    {
        $this->iterator->next();
    }

    /**
     * @return TKey
     */
    public function key()
    {
        return $this->iterator->key();
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->iterator->rewind();
    }
}
