<?php


namespace Hammie\Algorithms\Structure;

/**
 * @template TKey
 * @template TValue
 */
class KeyValuePair
{
    /**
     * @param TKey $key
     * @param TValue $value
     */
    public function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function __clone()
    {
        $this->key = clone $this->key;
        $this->value = clone $this->value;
    }

    /**
     * @var TKey
     */
    public $key;

    /**
     * @var TValue
     */
    public $value;
}
